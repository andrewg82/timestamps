package ee.neotech.repository.impl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import ee.neotech.config.MySQLProperties;
import ee.neotech.domain.TimestampEntity;
import ee.neotech.repository.Repository;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public @Singleton
class TimestampEntityRepository implements Repository<TimestampEntity> {
    private final static String INSERT_QUERY = "INSERT INTO timestamps(timestamp) VALUES (?)";
    private final static String GET_ALL_QUERY = "SELECT * FROM timestamps";
    private final static String CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS timestamps (" +
            "id INT(11) NOT NULL AUTO_INCREMENT," +
            "timestamp DATETIME(3) NOT NULL," +
            "PRIMARY KEY (id))";

    private static Connection connection;

    private Logger logger;

    {
        try {
            Class.forName(MySQLProperties.DATABASE_DRIVER).newInstance();
            connect(false);
            if (null != connection) {
                createTimestampsTable();
            }
        } catch (Exception connectionException) {
            System.out.println("There are some issues with initial connection with DB: " + connectionException.getMessage());
        }
    }

    @Inject
    public TimestampEntityRepository(Logger logger) {
        this.logger = logger;
    }

    @Override
    public Boolean isConnected() {
        try {
            return (null != connection && !connection.isClosed() && connection.isValid(MySQLProperties.DATABASE_CONNECTION_TIMEOUT_SEC));
        } catch (SQLException sqlException) {
            return false;
        }
    }

    @Override
    public Integer insert(TimestampEntity entity) {
        boolean successInsertion = false;
        int result = 0;

        while (!successInsertion) {
            connect(true);
            try (PreparedStatement statement = connection.prepareStatement(INSERT_QUERY)) {
                statement.setTimestamp(1, entity.getTimestamp());
                result = statement.executeUpdate();
                successInsertion = true;
            } catch (SQLException sqlException) {
                logger.severe("Exception occurred while executing query: "
                        .concat(sqlException.getMessage()));
            }
        }
        return result;
    }

    @Override
    public List<TimestampEntity> getAll() {
        List<TimestampEntity> timestampEntities = new LinkedList<>();
        connect(false);
        if (null != connection) {
            try (Statement statement = connection.createStatement();
                 ResultSet resultSet = statement.executeQuery(GET_ALL_QUERY)) {

                while (resultSet.next()) {
                    TimestampEntity timestampEntity = new TimestampEntity(
                            resultSet.getLong("id"),
                            resultSet.getTimestamp("timestamp")
                    );
                    timestampEntities.add(timestampEntity);
                }
            } catch (SQLException sqlException) {
                logger.severe("Exception occurred while executing query: "
                        .concat(sqlException.getMessage()));
            }
        }

        return timestampEntities;
    }

    private void connect(boolean tryToReconnect) {
        boolean successConnection = false;

        while (!successConnection) {
            try {
                if (!isConnected()) {
                    connection = DriverManager.getConnection(
                            MySQLProperties.DATABASE_URL,
                            MySQLProperties.DATABASE_USER,
                            MySQLProperties.DATABASE_PASSWORD
                    );
                }
                successConnection = true;
            } catch (Exception connectionException) {
                if (!tryToReconnect) {
                    logger.warning("There are some issues with DB connection: "
                            .concat(connectionException.getMessage()));
                    break;
                }

                logger.warning("There are some issues with DB connection: "
                        .concat(connectionException.getMessage())
                        .concat(" Trying to reconnect..."));
                try {
                    TimeUnit.SECONDS.sleep(MySQLProperties.DATABASE_CONNECTION_CHECKING_INTERVAL_SEC);
                } catch (InterruptedException interruptedException) {
                    logger.severe("DB Reconnection was interrupted: "
                            .concat(connectionException.getMessage()));
                }
            }
        }
    }

    private void createTimestampsTable() {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(CREATE_TABLE_QUERY);
        } catch (SQLException sqlException) {
            logger.severe("Exception occurred while executing query: ");
        }
    }
}
