package ee.neotech.repository;

import java.util.List;

public interface Repository<T> {
    Boolean isConnected();

    Integer insert(T entity);

    List<T> getAll();
}
