package ee.neotech.domain;

import java.sql.Timestamp;

public class TimestampEntity {
    private Long id;
    private Timestamp timestamp;

    public TimestampEntity(Long id, Timestamp timestamp) {
        this.id = id;
        this.timestamp = timestamp;
    }

    public Long getId() {
        return id;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }
}
