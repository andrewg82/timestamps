package ee.neotech;

import com.google.inject.Guice;
import com.google.inject.Injector;
import ee.neotech.config.DependenciesBinding;
import ee.neotech.service.TimestampService;
import ee.neotech.service.impl.DefaultTimestampService;

import static ee.neotech.config.LoggingProperties.DEFAULT_FORMAT;
import static ee.neotech.config.LoggingProperties.DEFAULT_FORMATTER;

public class Timestamps {

    public static void main(String[] args) {
        System.setProperty(DEFAULT_FORMATTER, DEFAULT_FORMAT);

        Injector injector = Guice.createInjector(new DependenciesBinding());
        TimestampService timestampService = injector.getInstance(DefaultTimestampService.class);

        if (args.length == 0) {
            timestampService.startProduceAndSave();
        } else if (args.length == 1 && args[0].toLowerCase().equals("-p")) {
            timestampService.showAll();
        } else {
            System.out.println("Wrong command line arguments!"
                    + " Run with -p to print all records from DB or without any parameters to start consume/save timestamps.");
        }
    }
}
