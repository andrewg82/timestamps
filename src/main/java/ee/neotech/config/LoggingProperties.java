package ee.neotech.config;

import java.util.ResourceBundle;

public class LoggingProperties {
    private static ResourceBundle loggingResourceBundle = ResourceBundle.getBundle("logging");

    public static final String DEFAULT_FORMATTER = loggingResourceBundle.getString("default-formatter");
    public static final String DEFAULT_FORMAT = loggingResourceBundle.getString("default-format");
}
