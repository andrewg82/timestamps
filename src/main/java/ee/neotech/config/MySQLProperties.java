package ee.neotech.config;

import java.util.ResourceBundle;

public class MySQLProperties {
    private static ResourceBundle mySqlResourceBundle = ResourceBundle.getBundle("db_mysql");

    public static final String DATABASE_DRIVER = mySqlResourceBundle.getString("database-driver");
    public static final String DATABASE_URL = mySqlResourceBundle.getString("database-url");
    public static final String DATABASE_USER = mySqlResourceBundle.getString("database-user");
    public static final String DATABASE_PASSWORD = mySqlResourceBundle.getString("database-password");

    public static final Integer DATABASE_CONNECTION_TIMEOUT_SEC = Integer.parseInt(mySqlResourceBundle.getString("database-connection-timeout-sec"));
    public static final Integer DATABASE_CONNECTION_CHECKING_INTERVAL_SEC = Integer.parseInt(mySqlResourceBundle.getString("database-connection-checking-interval-sec"));
}
