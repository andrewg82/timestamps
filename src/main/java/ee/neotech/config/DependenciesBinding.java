package ee.neotech.config;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;
import ee.neotech.domain.TimestampEntity;
import ee.neotech.repository.Repository;
import ee.neotech.repository.impl.TimestampEntityRepository;
import ee.neotech.service.AsyncService;
import ee.neotech.service.ReaderService;
import ee.neotech.service.TimestampService;
import ee.neotech.service.impl.DefaultTimestampProducerService;
import ee.neotech.service.impl.DefaultTimestampReaderService;
import ee.neotech.service.impl.DefaultTimestampSaverService;
import ee.neotech.service.impl.DefaultTimestampService;

import java.sql.Timestamp;

public class DependenciesBinding extends AbstractModule {

    @Override
    protected void configure() {
        bind(new TypeLiteral<Repository<TimestampEntity>>() {}).to(TimestampEntityRepository.class).in(Scopes.SINGLETON);
        bind(new TypeLiteral<AsyncService<Timestamp>>() {}).annotatedWith(Names.named("TimestampProducer")).to(DefaultTimestampProducerService.class);
        bind(new TypeLiteral<AsyncService<Timestamp>>() {}).annotatedWith(Names.named("TimestampSaver")).to(DefaultTimestampSaverService.class);
        bind(new TypeLiteral<ReaderService<TimestampEntity>>() {}).to(DefaultTimestampReaderService.class).in(Scopes.SINGLETON);
        bind(TimestampService.class).to(DefaultTimestampService.class).in(Scopes.SINGLETON);
    }
}
