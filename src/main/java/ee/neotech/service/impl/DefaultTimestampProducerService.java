package ee.neotech.service.impl;

import com.google.inject.Inject;
import ee.neotech.service.AsyncService;

import java.sql.Timestamp;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class DefaultTimestampProducerService implements AsyncService<Timestamp> {
    private BlockingQueue<Timestamp> destination = null;

    private volatile Boolean isStopped = false;
    private final static Integer PRODUCING_INTERVAL_SEC = 1;

    private Logger logger;

    @Inject
    public DefaultTimestampProducerService(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void stop() {
        isStopped = true;
    }

    @Override
    public void setQueue(BlockingQueue<Timestamp> queue) {
        this.destination = queue;
    }

    @Override
    public void run() {
        logger.info("Timestamp Producer was started!");

        if (null == destination) {
            logger.severe("Destination queue was not set!");
            stop();
        }

        while (!isStopped) {
            try {
                Timestamp currentTimestamp = produceTimestamp(destination);
                logger.info("Timestamp Producer added new timestamp to buffer: "
                        .concat(currentTimestamp.toString()));
                TimeUnit.SECONDS.sleep(PRODUCING_INTERVAL_SEC);
            } catch (InterruptedException interruptedException) {
                logger.severe("Timestamp Producer thread was interrupted: "
                        .concat(interruptedException.getMessage()));
            }
        }
    }

    private Timestamp produceTimestamp(BlockingQueue<Timestamp> destination) throws InterruptedException {
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        destination.put(currentTimestamp);
        return currentTimestamp;
    }
}
