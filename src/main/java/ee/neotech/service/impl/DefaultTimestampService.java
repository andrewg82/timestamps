package ee.neotech.service.impl;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import ee.neotech.domain.TimestampEntity;
import ee.neotech.service.AsyncService;
import ee.neotech.service.ReaderService;
import ee.neotech.service.TimestampService;

import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class DefaultTimestampService implements TimestampService {
    private final AsyncService<Timestamp> producerService;
    private final AsyncService<Timestamp> saverService;
    private final ReaderService<TimestampEntity> readerService;

    @Inject
    public DefaultTimestampService(
            @Named("TimestampProducer") AsyncService<Timestamp> producerService,
            @Named("TimestampSaver") AsyncService<Timestamp> saverService,
            ReaderService<TimestampEntity> readerService
    ) {
        this.producerService = producerService;
        this.saverService = saverService;
        this.readerService = readerService;
    }

    @Override
    public void startProduceAndSave() {
        BlockingQueue<Timestamp> exchangeBuffer = new LinkedBlockingQueue<>();
        producerService.setQueue(exchangeBuffer);
        saverService.setQueue(exchangeBuffer);

        Thread producerThread = new Thread(producerService);
        Thread saverThread = new Thread(saverService);
        producerThread.start();
        saverThread.start();
    }

    @Override
    public void showAll() {
        List<TimestampEntity> timestampEntities = readerService.getAll();
        if (null != timestampEntities && timestampEntities.size() > 0) {
            System.out.println(" id  |        timestamp        |");
            System.out.println("-----|-------------------------|");

            timestampEntities.forEach(entity -> {
                System.out.printf("%-4d | %-23s |%n", entity.getId(), entity.getTimestamp());
            });
        }
    }
}
