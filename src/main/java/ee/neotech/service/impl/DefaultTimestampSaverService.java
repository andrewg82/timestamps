package ee.neotech.service.impl;

import com.google.inject.Inject;
import ee.neotech.domain.TimestampEntity;
import ee.neotech.repository.Repository;
import ee.neotech.service.AsyncService;

import java.sql.Timestamp;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

public class DefaultTimestampSaverService implements AsyncService<Timestamp> {
    private final Repository<TimestampEntity> timestampEntityRepository;
    private BlockingQueue<Timestamp> source = null;

    private volatile Boolean isStopped = false;

    private Logger logger;

    @Inject
    public DefaultTimestampSaverService(
            Repository<TimestampEntity> timestampEntityRepository,
            Logger logger
    ) {
        this.timestampEntityRepository = timestampEntityRepository;
        this.logger = logger;
    }

    @Override
    public void stop() {
        isStopped = true;
    }

    @Override
    public void setQueue(BlockingQueue<Timestamp> queue) {
        this.source = queue;
    }

    @Override
    public void run() {
        logger.info("Timestamp Saver was started!");

        if (null == source) {
            logger.warning("Source queue was not set!");
            stop();
        }

        while (!isStopped) {
            try {
                Timestamp timestamp = source.take();
                logger.info("Timestamp Saver got new timestamp from buffer: "
                        .concat(timestamp.toString()));
                timestampEntityRepository.insert(new TimestampEntity(null, timestamp));
            } catch (InterruptedException interruptedException) {
                logger.severe("Timestamp Saver thread was interrupted: "
                        .concat(interruptedException.getMessage()));
            }
        }
    }
}
