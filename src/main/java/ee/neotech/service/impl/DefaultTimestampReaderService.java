package ee.neotech.service.impl;

import com.google.inject.Inject;
import ee.neotech.domain.TimestampEntity;
import ee.neotech.repository.Repository;
import ee.neotech.service.ReaderService;

import java.util.List;

public class DefaultTimestampReaderService implements ReaderService<TimestampEntity> {
    private final Repository<TimestampEntity> timestampEntityRepository;

    @Inject
    public DefaultTimestampReaderService(Repository<TimestampEntity> timestampEntityRepository) {
        this.timestampEntityRepository = timestampEntityRepository;
    }

    @Override
    public List<TimestampEntity> getAll() {
        return timestampEntityRepository.getAll();
    }
}
