package ee.neotech.service;

import java.util.List;

public interface ReaderService<T> {
    List<T> getAll();
}
