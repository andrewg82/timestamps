package ee.neotech.service;

import java.util.concurrent.BlockingQueue;

public interface AsyncService<T> extends Runnable {
    void stop();

    void setQueue(BlockingQueue<T> queue);
}
