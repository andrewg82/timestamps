package ee.neotech.service;

import ee.neotech.domain.TimestampEntity;
import ee.neotech.repository.Repository;
import ee.neotech.repository.impl.TimestampEntityRepository;
import ee.neotech.service.impl.DefaultTimestampSaverService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static org.mockito.Mockito.*;

public class DefaultTimestampSaverServiceTest {

    private AsyncService<Timestamp> timestampSaverService;
    private Repository<TimestampEntity> timestampEntityRepository;
    private BlockingQueue<Timestamp> queue;

    private final int WAITING_INTERVAL_SEC = 1;

    @Before
    public void init() {
        timestampEntityRepository = mock(TimestampEntityRepository.class);
        Logger logger = mock(Logger.class);
        timestampSaverService = new DefaultTimestampSaverService(timestampEntityRepository, logger);

        Thread timestampSaverServiceThread = new Thread(timestampSaverService);
        timestampSaverServiceThread.start();

        queue = new LinkedBlockingQueue<>();
    }

    @Test
    public void checkIfGetTimestampFromQueue() throws InterruptedException {
        timestampSaverService.setQueue(queue);

        queue.put(new Timestamp(System.currentTimeMillis()));
        TimeUnit.SECONDS.sleep(WAITING_INTERVAL_SEC);
        Assert.assertTrue(queue.isEmpty());
    }

    @Test
    public void checkIfDBInsertionCalled() throws InterruptedException {
        timestampSaverService.setQueue(queue);

        queue.put(new Timestamp(System.currentTimeMillis()));
        TimeUnit.SECONDS.sleep(WAITING_INTERVAL_SEC);
        verify(timestampEntityRepository, times(1)).insert(any(TimestampEntity.class));
    }
}
