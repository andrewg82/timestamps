package ee.neotech.service;

import ee.neotech.domain.TimestampEntity;
import ee.neotech.service.impl.DefaultTimestampProducerService;
import ee.neotech.service.impl.DefaultTimestampReaderService;
import ee.neotech.service.impl.DefaultTimestampSaverService;
import ee.neotech.service.impl.DefaultTimestampService;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;

import static org.mockito.Mockito.*;

public class DefaultTimestampServiceTest {
    private AsyncService<Timestamp> timestampProducerService;
    private AsyncService<Timestamp> timestampSaverService;
    private ReaderService<TimestampEntity> timestampReaderService;
    private TimestampService timestampService;


    @Before
    public void init() {
        timestampProducerService = mock(DefaultTimestampProducerService.class);
        timestampSaverService = mock(DefaultTimestampSaverService.class);
        timestampReaderService = mock(DefaultTimestampReaderService.class);

        timestampService = new DefaultTimestampService(
                timestampProducerService,
                timestampSaverService,
                timestampReaderService
        );
    }

    @Test
    public void startProduceAndSaveCheckIfMethodsCalled() {
        timestampService.startProduceAndSave();

        verify(timestampProducerService, times(1)).setQueue(any());
        verify(timestampSaverService, times(1)).setQueue(any());
    }

    @Test
    public void showAllCheckIfMethodCalled() {
        timestampService.showAll();

        verify(timestampReaderService, times(1)).getAll();
    }
}
