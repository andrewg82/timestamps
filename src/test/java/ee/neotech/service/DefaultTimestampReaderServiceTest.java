package ee.neotech.service;

import ee.neotech.domain.TimestampEntity;
import ee.neotech.repository.Repository;
import ee.neotech.repository.impl.TimestampEntityRepository;
import ee.neotech.service.impl.DefaultTimestampReaderService;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class DefaultTimestampReaderServiceTest {
    private ReaderService<TimestampEntity> timestampReaderService;
    private Repository<TimestampEntity> timestampEntityRepository;

    @Before
    public void init() {
        timestampEntityRepository = mock(TimestampEntityRepository.class);
        timestampReaderService = new DefaultTimestampReaderService(timestampEntityRepository);
    }

    @Test
    public void checkIfDBGetAllCall() {
        timestampReaderService.getAll();
        verify(timestampEntityRepository, times(1)).getAll();
    }
}
