package ee.neotech.service;

import ee.neotech.service.impl.DefaultTimestampProducerService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static org.mockito.Mockito.mock;

public class DefaultTimestampProducerServiceTest {
    private AsyncService<Timestamp> timestampProducerService;

    @Before
    public void init() {
        Logger logger = mock(Logger.class);
        timestampProducerService = new DefaultTimestampProducerService(logger);

        Thread timestampProducerServiceThread = new Thread(timestampProducerService);
        timestampProducerServiceThread.start();
    }

    @Test
    public void checkIfTimestampProduced() throws InterruptedException {
        final long WAITING_INTERVAL_MILLIS = 500L;
        final BlockingQueue queue = new LinkedBlockingQueue();

        timestampProducerService.setQueue(queue);
        TimeUnit.MILLISECONDS.sleep(WAITING_INTERVAL_MILLIS);
        timestampProducerService.stop();

        Object timestamp = queue.take();

        Assert.assertTrue(timestamp instanceof Timestamp);
    }

    @Test
    public void checkAmountOfProducedTimestamps() throws InterruptedException {
        final int WAITING_INTERVAL_SEC = 5;
        final BlockingQueue<Timestamp> queue = new LinkedBlockingQueue<>();

        timestampProducerService.setQueue(queue);
        TimeUnit.SECONDS.sleep(WAITING_INTERVAL_SEC);
        timestampProducerService.stop();

        Assert.assertEquals(queue.size(), WAITING_INTERVAL_SEC);
    }
}
