package ee.neotech.repository;

import ee.neotech.domain.TimestampEntity;
import ee.neotech.repository.impl.TimestampEntityRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.logging.Logger;

import static org.mockito.Mockito.mock;

public class TimestampEntityRepositoryTest {

    private Repository<TimestampEntity> timestampEntityRepository;

    @Before
    public void init() {
        Logger logger = mock(Logger.class);
        timestampEntityRepository = new TimestampEntityRepository(logger);
    }

    @Test
    public void dbHealthCheck() {
        Assert.assertTrue(timestampEntityRepository.isConnected());
    }
}
